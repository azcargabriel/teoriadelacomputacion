import java.util.ArrayList;

public class AFD {

  private boolean reconocido;
  
  public AFD(AFND afnd, String cadena) {
    reconocido = reconocer(afnd, cadena);
  }
  
  public boolean getValue() {
    return reconocido;
  }
  
  /**
   * Reconoce si una cadena es aceptada o no.
   * @param afnd
   * @param cadena
   * @return true si lo es, false en caso contrario
   */
  public boolean reconocer(AFND afnd, String cadena) {
    ArrayList<String> actual = new ArrayList<String>();
    actual = afnd.nodosIniciales;
    ArrayList<String> llegada = new ArrayList<String>();
    for (int i = 0; i < cadena.length(); i++) {
      if(!afnd.getLenguaje().contains(String.valueOf(cadena.charAt(i)))) {
        actual = afnd.nodosIniciales;
        continue;
      }
      if (actual.contains("final")) {
        return true;
      } else {
        String caracter = String.valueOf(cadena.charAt(i));
        for (int j = 0; j < actual.size(); j++) {
          NodoAFND auxafnd = afnd.value.encontrar(afnd.value, actual.get(j));
          if(auxafnd == null) continue;
          Estado aux = new Estado(auxafnd, caracter);
          llegada.addAll(aux.getEstados());
          llegada = filtrar(llegada);
        }
        llegada = filtrar(llegada);
        actual = llegada;
        if (actual.contains("final")) {
          return true;
        }
      }
    }    
   return false; 
  }
  
  /**
   * Elimina los duplicados del arreglo a.
   * @param a
   * @return el arreglo sin duplicados
   */
  public ArrayList<String> filtrar(ArrayList<String> a) {
    ArrayList<String> aux = new ArrayList<String>();
    for (int i = 0; i < a.size(); i++) {
      if(!aux.contains(a.get(i))) {
        aux.add(a.get(i));
      }
    }
    return aux;
  }
}
