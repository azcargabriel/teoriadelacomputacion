import java.util.ArrayList;
import java.util.Stack;

/**
 * Clase que crea un AFND.
 *
 */
public class AFND {
  
  int actualIndex;
  ArrayList<String> lenguaje;
  ArrayList<String> nodosIniciales;
  NodoAFND value;  
  
  public AFND() {
    actualIndex = -1;
    value = new NodoAFND();
  }
  
  /**
   * Constructor que crea un AFND a partir de una ER.
   * @param er la expresion regular.
   */
  public AFND(String er) {
    lenguaje = new ArrayList<String>();
    Stack<NodoAFND> values = new Stack<NodoAFND>();
    Stack<String> operators = new Stack<String>();
    er = er + " ";
    for (int i = 0; i < er.length(); i++) {
      if (er.charAt(i) == '(') {
        continue;
      } else if (er.charAt(i) == ')' || er.charAt(i) == ' ') {
        if(operators.isEmpty()) {
          break;
        } else {
          String operator = operators.pop();
          if(operator.charAt(0) == '*') {
            NodoAFND newNodo = generar(values.pop(), null, operator);
            values.push(newNodo);
            i--;
            continue;
            } else {
              NodoAFND b = values.pop();
              NodoAFND a = values.pop();
              NodoAFND newNodo = generar(a, b, operator);
              values.push(newNodo);
              i--;
              continue;
              }
          }
      } else if (er.charAt(i) == '.' || er.charAt(i) == '*' || er.charAt(i) == '|') {
        operators.push(String.valueOf(er.charAt(i)));
        continue;
      } else {
        lenguaje.add(String.valueOf(er.charAt(i)));
        values.push(oneChar(String.valueOf(er.charAt(i))));
        continue;
      }
    }
    NodoAFND result = values.pop();
    this.actualIndex = 0;
    renombrar(result);
    value = result;
    Estado e = new Estado(value, "");
    nodosIniciales = e.getEstados();
  }
  
  /**
   * Crea un AFND a partir de un solo caracter.
   * @param str caracter a transformar.
   * @return en AFND generado.
   */
  public NodoAFND oneChar(String str) {
    actualIndex++;
    return new NodoAFND(str, Integer.toString(actualIndex));
  }
  
  /**
   * Genera un el AFND que sale al hacer Th(a|b).
   * @param a un AFND.
   * @param b un AFND.
   * @return El AFND Th(a|b).
   */
  public NodoAFND union(NodoAFND a, NodoAFND b) {
    NodoAFND finale = new NodoAFND(true);
    a.enlazarFinal(finale);
    b.enlazarFinal(finale);
    NodoAFND inicio = new NodoAFND();
    inicio.enlazar(a, "");
    inicio.enlazar(b, "");
    return inicio;
  }
  
  /**
   * Genera un el AFND que sale al hacer Th(a.b).
   * @param a un AFND.
   * @param b un AFND.
   * @return El AFND Th(a.b).
   */
  public NodoAFND concatenar(NodoAFND a, NodoAFND b) {
    a.enlazarFinal(b);
    return a;
  }
  
  /**
   * Genera un el AFND que sale al hacer Th(a*).
   * @param a un AFND.
   * @return El AFND Th(a*).
   */
  public NodoAFND kleene(NodoAFND a) {
    NodoAFND finale = new NodoAFND(true);
    a.enlazarFinal(finale);
    a.enlazar(a, "");
    NodoAFND inicio = new NodoAFND();
    inicio.enlazar(a, "");
    inicio.enlazar(finale, "");
    return inicio;
  }
  
  /**
   * Decide que operacion hacer en funcion de operador.
   * @param a un AFND.
   * @param b un AFND.
   * @param operador decide que operacion hacer.
   * @return el nodo Th(a operacion b).
   */
  public NodoAFND generar(NodoAFND a, NodoAFND b, String operador) {
    if(operador.equals("|")) {
      return union(a,b);
    } else if (operador.equals(".")) {
      return concatenar(a,b);
    } else if (operador.equals("*")) {
      return kleene(a);
    }
    return null;
  }
  
  public ArrayList<String> getLenguaje() {
    return lenguaje;
  }
  
  /**
   * Coloca nombres a los estados el AFND a. Empezando desde 0.
   * los estados finales se llaman "final".
   * Si hay mas de un estado final, elimina los que no son finales en realidad.
   * @param a AFND que se le renombraran sus nodos.
   */
  public void renombrar(NodoAFND a) {
    if (a == null || a.getVisitado()) {
      return;
    }
    
    a.visit();
    
    if (a.getAfnd1() == null) {
      a.setTrans(1, "//");
    }
    if (a.getAfnd2() == null) {
      a.setTrans(2,"//");
    }
    
    if((a.getAfnd1() != null || a.getAfnd2() != null)) {
      a.rename(String.valueOf(this.actualIndex));
      actualIndex++;
    }
    
    renombrar(a.getAfnd1());
    renombrar(a.getAfnd2());
  }
}
