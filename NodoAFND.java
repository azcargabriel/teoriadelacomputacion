
/**
 * Clase que describe un estado de un AFND
 *
 */
public class NodoAFND {
  private String nombre;
  private String trans1;
  private String trans2;
  private NodoAFND afnd1;
  private NodoAFND afnd2;
  private boolean visitado;
  
  /**
   * Constructor.
   */
  public NodoAFND() {
    nombre = "";
    trans1 = "";
    trans2 = "";
    afnd1 = null;
    afnd2 = null;
    visitado = false;
  }
  
  /**
   * Constructor que crea un estado final.
   * @param finale indica que hay que crear un estado final.
   */
  public NodoAFND(boolean finale) {
    nombre = "final";
    trans1 = "";
    trans2 = "";
    afnd1 = null;
    afnd2 = null;
    visitado = false;
  }
  
  /**
   * Genera un estado donde con a se llega a un estado final.
   * @param a caracter a leer.
   * @param nombre del estado.
   */
  public NodoAFND(String a, String nombre) {
    this.nombre = nombre;
    trans1 = a;
    trans2 = "";
    afnd1 = new NodoAFND(true);
    afnd2 = null;
    visitado = false;
  }
  
  /**
   * Indica si el nodo ha sido visitado.
   * Se usa para cambiarle el nombres a los nodos.
   * @return visitado.
   */
  public boolean getVisitado() {
    return visitado;
  }
  
  /**
   * Entrega el caracter que nos lleva al estado afnd1.
   * @return caracter de lectura.
   */
  public String getTrans1() {
    return trans1;
  }
  
  /**
   * Entrega el caracter que nos lleva al estado afnd2.
   * @return caracter de lectura.
   */
  public String getTrans2() {
    return trans2;
  }
  
  /**
   * Entrega el estado afnd1.
   * @return afnd1.
   */
  public NodoAFND getAfnd1() {
    return afnd1;
  }
  
  /**
   * Entrega el estado afnd2.
   * @return afnd2.
   */
  public NodoAFND getAfnd2() {
    return afnd2;
  }
  
  /**
   * Retorna el nombre del estado.
   * @return nombre.
   */
  public String getName() {
    return nombre;
  }
  
  /**
   * Cambiar el valor de visitado a true.
   */
  public void visit() {
    visitado = true;
  }
  
  /**
   * Le cambia el valor al estado.
   * @param name recibe el nuevo nombre a colocar.
   */
  public void rename(String name) {
    this.nombre = name;
  }
  
  /**
   * Le cambia el valor a la transicion que indica i, por nuevo.
   * @param i transicion a cambiar.
   * @param nuevo valor nuevo a colocar.
   */
  public void setTrans(int i, String nuevo) {
    if(i == 1) {
      trans1 = nuevo;
    } else {
      trans2 = nuevo;
    }
  }
  
  /**
   * Enlaza el estado actual a otro, a leer el caracter trans.
   * @param nodo nodo a enlazar.
   * @param trans si leemos este caracter vamos a nodo.
   */
  public void enlazar(NodoAFND nodo, String trans) {
    if (afnd1 == null) {
      trans1 = trans;
      afnd1 = nodo;
    } else if (afnd2 == null) {
      trans2 = trans;
      afnd2 = nodo;
    } else {
      afnd1.enlazar(nodo,trans);
      afnd2.enlazar(nodo, trans);
    }
  }
  
  /**
   * Coloca al final del estado el nodo.
   * @param nodo final.
   */
  public void enlazarFinal(NodoAFND nodo) {
    if (afnd1 == null && afnd2 == null) {
      trans1 = "";
      afnd1 = nodo;
    } else if (afnd1 != null) {
      afnd1.enlazarFinal(nodo);
    } else if(afnd2 != null) {
      afnd2.enlazarFinal(nodo);
    }
  }
  
  /**
   * Encuentra un estado en las referencias de otro.
   * @param nodo donde buscaremos.
   * @param nombre del estado que buscamos.
   * @return el nodo, si fue encontrado. null si no.
   */
  public NodoAFND encontrar(NodoAFND nodo, String nombre) {
    if (nodo == null) {
      return null;
    }
    
    if (nodo.getName().equals(nombre)) {
      return nodo;
    } else {
      NodoAFND uno = encontrar(nodo.afnd1, nombre);
      NodoAFND dos = encontrar(nodo.afnd2, nombre);
      if(uno != null) {
        return uno;
      } else if (dos != null) {
        return dos;
      } else {
        return null;
      } 
    }
  }
}
