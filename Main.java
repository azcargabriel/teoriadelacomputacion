import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

  public static void main(String[] args) throws IOException {
    
    InputStreamReader flujo = new InputStreamReader(System.in);
    BufferedReader leer = new BufferedReader(flujo);
    
    System.out.println("Introduzca la Expresion Regular");
    String er = leer.readLine();

    AFND afnd = new AFND(er);
    
    System.out.println("Coloque el nombre del texto a leer en formato 'nombre.txt'");
    
    String txt = leer.readLine();
    BufferedReader bf = new BufferedReader(new FileReader(txt));
    String linea = null;
    while ((linea = bf.readLine()) != null) {
      AFD uno = new AFD(afnd, linea);
      if(uno.getValue() == true) {
        System.out.println(linea);
      }
   }
    bf.close();
  }
}
