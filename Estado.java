import java.util.ArrayList;

/**
 * Clase que describe los estados a los que puede llevar un estado a través de un caracter.
 * 
 */
public class Estado {
  private String nombre;
  private ArrayList<String> estados;
  
  /**
   * Constructor del arreglo de estados a través de un estado.
   * @param afnd a buscar.
   * @param trans leyendo este caracter.
   */
  public Estado(NodoAFND afnd, String trans) {
    nombre = "default";
    estados = new ArrayList<String>();
    buscarAlcanzables(afnd, estados, trans);
  }
  
  /**
   * Entrega el nombre del estado.
   * @return nombre.
   */
   public String getNombre() {
     return nombre;
   }
  
   /**
    * Entrega el arreglo de estados.
    * @return estados.
    */
    public ArrayList<String> getEstados() {
      return estados;
    }
   
   
   /**
    * Setea el nombre del estado.
    * @param newNombre nuevo nombre del estado.
    */
   public void setNombre(String newNombre) {
     nombre = newNombre;
   }
   
  /**
   * Encuentra todos los nombres de los estados a los cuales puede llegar afnd leyendo tran.
   * @param afnd a buscar.
   * @param act arreglo donde se guardan los nombres de los estados alcanzados.
   * @param tran caracter a leer.
   */
  public void buscarAlcanzables(NodoAFND afnd, ArrayList<String> act, String tran) {
  
    String tr1 = afnd.getTrans1();
    String tr2 = afnd.getTrans2();
    NodoAFND afnd1 = afnd.getAfnd1();
    NodoAFND afnd2 = afnd.getAfnd2();
    
    if (act.contains(afnd.getName())) {
      return;
    }
    
    act.add(afnd.getName());
    
    //Vemos que podemos alcanzar desde afnd1
    if (tr1 != "//") {
      if (tr1.equals(tran)) {
        buscarAlcanzables(afnd1, act, "");
      }
    }
    
    //Vemos que podemos alcanzar desde afnd2
    if (tr2 != "//") {
      if (tr2.equals(tran)) {
        buscarAlcanzables(afnd2, act, "");
      }
    }
    
    return;
   }
  
  /**
   * Filtra los valores que no deberían estar.
   */
  public void filtrarEstados(ArrayList<String> estados) {
    
  }
}
